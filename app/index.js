const { desktopCapturer } = require('electron');
const os = require('os');
const AudioContext = window.AudioContext || window.webkitAudioContext;
const platforms = {
	WINDOWS: 'WINDOWS',
	MAC: 'MAC',
	LINUX: 'LINUX',
	SUN: 'SUN',
	OPENBSD: 'OPENBSD',
	ANDROID: 'ANDROID',
	AIX: 'AIX',
};

const platformsNames = {
	win32: platforms.WINDOWS,
	darwin: platforms.MAC,
	linux: platforms.LINUX,
	sunos: platforms.SUN,
	openbsd: platforms.OPENBSD,
	android: platforms.ANDROID,
	aix: platforms.AIX,
};

const currentPlatform = platformsNames[os.platform()];

desktopCapturer.getSources({ types: ['screen'] }).then(async sources => {
	try {
		handleSources(sources);
	} catch (e) {
		handleError(e)
	}
	return;
})

try {
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
} catch (e) {
	log('Web Audio API not supported.');
}

function mixTracks(tracks) {
	var ac = new AudioContext();
	var dest = ac.createMediaStreamDestination();
	for (var i = 0; i < tracks.length; i++) {
		if (tracks[i]) {
			const source = ac.createMediaStreamSource(new MediaStream([tracks[i]]));
			source.connect(dest);
		}
	}
	return dest.stream.getTracks()[0];
}

const initializeAudioAnalyser = (track) => {
	const audioContext = new AudioContext();
	const source = audioContext.createMediaStreamSource(new MediaStream([track]));
	const analyser = audioContext.createAnalyser();
	source.connect(analyser);
	return analyser;
}

async function handleSources(sources) {
	try {
		const mediaStream = await (currentPlatform == platforms.MAC ? getMacAudioStream() : getNormalAudioStream(sources));
		console.log(mediaStream);
		var audioTracks = mediaStream.getAudioTracks();
		const analyser = initializeAudioAnalyser(audioTracks[0]);
		function getFrequencyData() {
			const bufferLength = analyser.frequencyBinCount;
			const amplitudeArray = new Uint8Array(bufferLength);
			analyser.getByteFrequencyData(amplitudeArray);
			// we're ready to receive some data!
			var canvas = document.getElementById('canvas'),
				cwidth = canvas.width,
				cheight = canvas.height - 2,
				meterWidth = 10, //width of the meters in the spectrum
				gap = 2, //gap between meters
				capHeight = 2,
				capStyle = '#fff',
				meterNum = 800 / (10 + 2), //count of the meters
				capYPositionArray = []; ////store the vertical position of hte caps for the preivous frame
			ctx = canvas.getContext('2d'),
				gradient = ctx.createLinearGradient(0, 0, 0, 300);
			gradient.addColorStop(1, '#0f0');
			gradient.addColorStop(0.5, '#ff0');
			gradient.addColorStop(0, '#f00');
			var step = Math.round(amplitudeArray.length / meterNum); //sample limited data from the total array
			ctx.clearRect(0, 0, cwidth, cheight);
			for (var i = 0; i < meterNum; i++) {
				var value = amplitudeArray[i * step];
				if (capYPositionArray.length < Math.round(meterNum)) {
					capYPositionArray.push(value);
				};
				ctx.fillStyle = capStyle;
				//draw the cap, with transition effect
				if (value < capYPositionArray[i]) {
					ctx.fillRect(i * 12, cheight - (--capYPositionArray[i]), meterWidth, capHeight);
				} else {
					ctx.fillRect(i * 12, cheight - value, meterWidth, capHeight);
					capYPositionArray[i] = value;
				};
				ctx.fillStyle = gradient; //set the filllStyle to gradient for a better look
				ctx.fillRect(i * 12 /*meterWidth+gap*/, cheight - value + capHeight, meterWidth, cheight); //the meter
			}
		}

		function runSpectrum() {
			getFrequencyData();
			requestAnimationFrame(runSpectrum)
		}

		runSpectrum();
	} catch (e) {
		handleError(e)
	}
}

function handleError(e) {
	console.log(e)
}

async function getMacAudioStream() {
	const devices = await navigator.mediaDevices.enumerateDevices();
	let audDevice = devices.filter(device => {
		return device.kind == "audiooutput" && device.label == "Soundflower (2ch)" && device.deviceId != "default"
	})[0];

	const res = await navigator.mediaDevices.getUserMedia({
		audio: {
			deviceId: { exact: audDevice.deviceId }
		},
		video: false
	});

	return res
}

async function getNormalAudioStream(sources) {
	let audioOnly = new MediaStream();

	for (let i = 0; i < sources.length; i++) {
		const stream = await navigator.mediaDevices.getUserMedia({
			audio: {
				mandatory: {
					chromeMediaSource: 'desktop',
					chromeMediaSourceId: sources[i].id,
					echoCancellation: true
				},
			},
			video: {
				mandatory: {
					chromeMediaSource: 'desktop',
					chromeMediaSourceId: sources[i].id,
					minWidth: 1280,
					maxWidth: 1280,
					minHeight: 720,
					maxHeight: 720
				}
			}
		});

		stream.getAudioTracks().forEach(function (track) {
			audioOnly.addTrack(track);

			// remove video track, because we are gonna record only speakers
			stream.removeTrack(track);
		});
	}

	return audioOnly;
}