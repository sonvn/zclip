const EventEmitter = require('events');

const clipboardEmitter = new EventEmitter();

let intervalID = null;

const clipboardEvent = {
  start: (delay = 100) => {
    intervalID = setInterval(() => {
      
    }, delay);
  },
  stop: () => {
    clearInterval(intervalID);
  },
  on: (event, listner) => {

  }
};