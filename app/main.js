const { app, BrowserWindow, screen, globalShortcut, ipcMain, clipboard, dialog } = require('electron');

const path = require('path');
const url = require('url');
// const installExtension = require('electron-devtools-installer');
// const { REACT_DEVELOPER_TOOLS } = installExtension;

const dev = true;

let mainWindow = null;
let title = 'Clipboard Manager Electron';

var knex = require("knex")({
	client: "sqlite3",
	connection: {
		filename: path.join(__dirname, 'database.sqlite')
	}
});

function createWindow() {
  // get screen info
  // var mainScreen = screen.getPrimaryDisplay();
  // var allScreens = screen.getAllDisplays();
  
  // console.log(mainScreen, allScreens);

  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    icon: path.join(__dirname, '/assets/logo.png'),
    frame: false,
    transparent : true,
    webPreferences: {
      nodeIntegration: true,
      experimentalFeatures: true,
    },
  });

  mainWindow.setMenu(null);

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  if (dev) {
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
    // const {
    //   default: installExtension,
    //   REACT_DEVELOPER_TOOLS,
    // } = require('electron-devtools-installer'); // eslint-disable-line
    // installExtension(REACT_DEVELOPER_TOOLS)
    //   .then(name => console.log(`Added Extension:  ${name}`))
    //   .catch(err => console.log('An error occurred: ', err));
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  mainWindow.once("ready-to-show", () => { mainWindow.show() })

	ipcMain.on("mainWindowLoaded", function () {
		let result = knex.select("FirstName").from("User")
		result.then(function(rows){
			mainWindow.webContents.send("resultSent", rows);
		})
	});
}



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow();
  // const ret = globalShortcut.register('CommandOrControl+Alt+0', () => {
  //   console.log('CommandOrControl+Alt+0 is pressed');
  //   createWindow();
  // })
  const ret = globalShortcut.register('CommandOrControl+c', (e) => {
    console.log(e)
    let text = clipboard.availableFormats();
    console.log(text);
    // console.log()
  });

  if (!ret) {
    console.log('registration failed')
  }

  // Check whether a shortcut is registered.
  console.log(globalShortcut.isRegistered('CommandOrControl+Alt+0'))
})

app.on('second-instance', () => {
  dialog.showMessageBox({
    type: 'info',
    title,
    message: 'An instance of ' + title + ' already open',
    buttons: ['Ok']
  })
})


// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.